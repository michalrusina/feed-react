/* Globals */


var	Router = ReactRouter,
	Route = Router.Route,
	Redirect = Router.Redirect,
	NotFoundRoute = Router.NotFoundRoute,
	RouteHandler = Router.RouteHandler,
	Link = Router.Link;


/* Helpers */


Helpers = {
	createUniqueArray: function(inputArray, sorter) {
		var arrResult = {},
			nonDuplicatedArray = [],
			i,
			n;

		for (i = 0, n = inputArray.length; i < n; i++) {
			var itm = inputArray[i];

			if (sorter) { arrResult[itm[sorter]] = itm;	}
			else { arrResult[itm] = itm; }
		}

		i = 0;

		for (var itm in arrResult) {
			nonDuplicatedArray[i++] = arrResult[itm];
		}

		return nonDuplicatedArray;
	},
	flattenSerializedArray: function(inputArray) {
		var arrResult = {};

		for (i = 0, n = inputArray.length; i < n; i++) {
			var itm = inputArray[i];

			arrResult[itm.name] = itm.value;
		}

		return arrResult;
	},
	isBrowser: function() {
		return (typeof(window) !== 'undefined');
	},
	getJson: function (url, query, callback) {
		superagent
			.get(url)
			.query(query)
			.end(function (error, res) {
				if (error) { return callback(error); }
				if (res.status != 200) { return callback('unexpected response code: ' + res.status); }
				if (!res.body) { return callback('no body'); }

				callback(null, res.body);
			});
	}
};


/* Mixins */


var MasonryMixin = {
	masonry: false,
	componentDidMount: function(domNode) {
		if (this.masonry || !Helpers.isBrowser) { return; }

		this.masonry = new Masonry(this.getDOMNode(), {
			itemSelector: '.feeditem',
			gutter: Data.config.gutter,
			columnWidth: Data.config.column,
			isFitWidth: true,
			transitionDuration: 0
		});

		this.getDOMNode().focus();
	},
	componentDidUpdate: function() {
		if (!Helpers.isBrowser) { return; }

		this.masonry.reloadItems();
		this.masonry.layout();
	}
};

var InfiniteMixin = {
	topPosition: function(domElement) {
		if (!domElement) { return 0; }

		return domElement.offsetTop + this.topPosition(domElement.offsetParent);
	},
	scrollListener: function () {
		var el = this.getDOMNode(),
			scrollTop =	(window.pageYOffset !== undefined) ? window.pageYOffset : (document.documentElement || document.body.parentNode || document.body).scrollTop;

		if (this.state.more && this.topPosition(el) + el.offsetHeight - scrollTop - window.innerHeight < 0) {
			this.detachScrollListener();
			this.loadMoreArticles();
		}

    },
	attachScrollListener: function() {
		if (!Helpers.isBrowser()) { return; }

		window.addEventListener('scroll', this.scrollListener);
		window.addEventListener('resize', this.scrollListener);
	},
	detachScrollListener: function() {
		if (!Helpers.isBrowser()) { return; }

		window.removeEventListener('scroll', this.scrollListener);
		window.removeEventListener('resize', this.scrollListener);
	},
	componentDidMount: function() {
		this.attachScrollListener();
	},
	componentDidUpdate: function() {
		this.attachScrollListener();
	},
	componentWillUnmount: function() {
		this.detachScrollListener();
	}
};

var IntervalMixin = {
	componentWillMount: function() { // cleanup before mounting
		this.intervals = [];
	},
	setInterval: function() {
		if (arguments[1]) {
			this.intervals.push(setInterval.apply(null, arguments));
		} else {
			arguments[0]();
		}
	},
	componentWillUnmount: function() { // cleanup before unmounting
		this.intervals.map(clearInterval);
	}
};

var LinkMixin = {
	linkText: function(url, text) {
		if (url) {
			return(
				<a href={url}>{text}</a>
			);
		} else {
			return(
				<span>{text}</span>
			);
		}
	}
};


/* SocialFeed */


var SocialFeed = React.createClass({
	render: function() {
		return (
			<div className="socialfeed">
				<RouteHandler {...this.props} />
			</div>
		);
	}
});


/* ErrorNotfound */


var ErrorNotfound = React.createClass({
	render: function() {
		return (
			<div className="errornotfound">
				<h1>{Data.locale.error}</h1>
				<p>{Data.locale.contentNotFound}</p>
			</div>
		);
	}
});


/* FeedBox */


var FeedBox = React.createClass({
	render: function() {
		return (
			<div className="feedbox">
				<FeedFilter />
				<FeedList filter={this.props.params.filter} />
			</div>
		);
	}
});


/* FeedList */


var FeedList = React.createClass({
	mixins: [MasonryMixin, InfiniteMixin],
	loadMoreArticles: function() {
		var page = this.state.page,
			perPage = Data.config.articlesPerPage;

		this.getArticlesFromServer({
			filter: this.props.filter,
			page: page,
			perPage: perPage
		});
	},
	getArticlesFromServer: function(request) {
		if (this.isMounted()) {
			Helpers.getJson(Data.config.articlesRestService, request, function(error, response) {
				this.includeLoadedArticles(request.page, response.body);
			}.bind(this));
		}
	},
	includeLoadedArticles: function(page, articles) {
		this.setState({
			page: page + 1,
			articles: Helpers.createUniqueArray(this.state.articles.concat(articles), 'id'),
			more: articles.length === Data.config.articlesPerPage
		});
	},
	getArticlesToRender: function() {
		return this.state.articles.map(function(article) {
			return (
				<FeedItem article={article} key={article.id} />
			);
		});
	},
	getMoreButtonToRender: function() {
		return (
			<div className="more">
				<MoreButton text={Data.locale.loadMoreArticles} onClick={this.loadMoreArticles} />
			</div>
		);
	},
	getInitialState: function() {
		return {
			page: 0,
			articles: [],
			more: true
		};
	},
	componentWillReceiveProps: function(nextProps) {
		if (nextProps.filter !== this.props.filter) {
			this.setState({
				filter: nextProps.filter,
				page: 0,
				articles: [],
				more: true
			}, function() { // setstate is asynchronous, so we need to use this callback
				this.loadMoreArticles();
			});
		}
	},
	componentDidMount: function() {
		this.loadMoreArticles();
	},
	render: function() {
		return (
			<div className="feedlist">
				{this.getArticlesToRender()}
			</div>
		); // gutter and column for responsive masonry sizing with css
	}
});


/* FeedFilter */


var FeedFilter = React.createClass({
	mixins: [Router.Navigation],
	filterChange: function(event) {
		this.transitionTo('feed', formData);
	},
	getInputsToRender: function() {
		return Data.filters.map(function(filter) {
			return (
				<Link className={filter.type} to="feed" params={{filter: filter.type}}>
					<Icon icon={filter.type} />
					{filter.name}
				</Link>
			);
		});
	},
	render: function() {
		return (
			<form className="feedfilter" onChange={this.filterChange}>
				{this.getInputsToRender()}
			</form>
		);
	}
});


/* FeedItem */


var FeedItem = React.createClass({
	resizeImage: function(image) {
		image.newHeight = Data.config.column * (image.width/image.height)

		return image;
	},
	render: function() {
		var article = this.props.article,
			feature = this.resizeImage(article.feature);

		return (
			<article className="feeditem">
				<figure className="articleimage">
					<Bitmap className="image" src={feature.url} alt={article.title} width={feature.width} height={feature.height} />
				</figure>
				<header className="articleheader">
					<Icon icon={article.type} />
					<Title title={article.title} url={article.url} />
					<Author name={article.author.name} url={article.author.url} />
					<Posted posted={article.posted} />
				</header>
				<div className="articlecontent">{article.content}</div>
			</article>
		);
	}
});


/* MoreButton */


var MoreButton = React.createClass({
	render: function() {
		return (
			<button className="morebutton" type={this.props.type} onClick={this.props.onClick}>
				<Icon icon="more" />
				{this.props.text}
			</button>
		);
	}
});


/* Icon */


var Icon = React.createClass({
	render: function() {
		var className = 'icon icon-' + this.props.icon;

		return (
			<i className={className}></i>
		);
	}
});


/* Title */


var Title = React.createClass({
	mixins: [LinkMixin],
	render: function() {
		return (
			<h1 className="title">
				{this.linkText(this.props.url, this.props.title)}
			</h1>
		);
	}
});


/* Author */


var Author = React.createClass({
	mixins: [LinkMixin],
	render: function() {
		return (
			<cite className="author">
				{this.linkText(this.props.url, this.props.name)}
			</cite>
		);
	}
});


/* Bitmap */


var Bitmap = React.createClass({
	onError: function () {
		this.getDOMNode().remove(); // remove image if image or link are broken
	},
	componentDidMount: function () {
		this.getDOMNode().src = this.props.src; // reload src to force onerror to be called if image link was not valid
	},
	render: function() {
		var style = {
			width: this.props.width,
			height: this.props.height
		};

		return (
			<div className={this.props.className}>
				<img style={style} src={this.props.src} alt={this.props.alt} onError={this.onError} />
			</div>
		);
	}
});


/* Posted */


var Posted = React.createClass({
	mixins: [IntervalMixin],

	updateText: function() {
		this.forceUpdate(); // hack to force updating as soon as props are received without using states
	},
	componentDidMount: function() {
		this.setInterval(this.updateText, (this.props.format) ? 0 : Data.config.postedUpdateInterval);
	},
	render: function() {
		var	posted = new Date(this.props.posted),
			momentObj = moment(this.props.posted),
			text = this.props.format ? {text: momentObj.format(this.props.format)} : {text: momentObj.fromNow()};;

		return (
			<time className="posted" title={posted.toLocaleString()} dateTime={posted}>{text}</time>
		);
	}
});


/* Router */


Router.run(
	(
		<Route handler={SocialFeed} path="/">
			<Redirect from="/" to="feed" params={{filter: 'all'}} />
			<Route name="feed" path="feed/:filter" handler={FeedBox} />
			<NotFoundRoute handler={ErrorNotfound} />
		</Route>
	),
	function(Handler, state) {
		React.render(<Handler params={state.params} />, document.body);
	}
);
