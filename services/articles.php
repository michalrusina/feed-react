<?php
	header('Content-Type: application/json');

	$page = $_REQUEST['page'];
	$perpage = $_REQUEST['perPage'];
	$filter = $_REQUEST['filter'];


	$i = 0;
	$articles = [];
	$types = ($filter == 'all') ? array('facebook', 'twitter', 'instagram', 'youtube') : array($filter);

	for ($i = 1; $i <= $perpage; $i++) {
		$id = $i + $page * $perpage;
		$type = (count($types) == 1) ? $types[0] : $types[rand(0, count($types) - 1)];
		$height = rand(100, 500);

		$article = array(
			'id' => $id,
			'type' => $type,
			'url' => 'http://article_url',
			'title' => 'Article ' . $type . $id,
			'posted' => rand(time() - 3600, time()) * 1000,
			'content' => 'Content',
			'author' => array(
				'name' => 'Author',
				'url' => 'http://author_url'
			),
			'feature' => array(
				'type' => 'image',
				'width' => 300,
				'height' => $height,
				'url' => 'http://placehold.it/300x' . $height
			)
		);

		$articles[] = $article;
	}

	$response = array(
		'body' => $articles,
		'status' => 200
	);

	echo json_encode($response);
?>
