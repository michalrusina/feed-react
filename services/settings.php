<?php
	header('Content-Type: application/json');

	$response = array(
		'config' => array(
			'articlesRestService' => 'services/articles.php',
			'articlesPerPage' => 20,
			'articleRestService' => 'services/article.php',
			'postedUpdateInterval' => 60000,
			'gutter' => 20,
			'column' => 300,
		),
		'locale' => array(
			'loadMoreArticles' => 'Load more articles',
			'error' => 'Error!',
			'contentNotFound' => 'Content not found.',
			'close' => 'Close'
		),
		'filters' => array(
			array(
				'type' => 'all',
				'name' => 'All'
			),
			array(
				'type' => 'facebook',
				'name' => 'Facebook'
			),
			array(
				'type' => 'twitter',
				'name' => 'Twitter'
			),
			array(
				'type' => 'instagram',
				'name' => 'Instagram'
			),
			array(
				'type' => 'youtube',
				'name' => 'YouTube'
			)
		)
	);

	echo 'Data = ' . json_encode($response);
?>